package org.sample.SpringEndpointSample;

import org.junit.jupiter.api.Test;
import org.sample.SpringEndpointSample.constants.EndPoints;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.web.bind.annotation.RequestMethod;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
class SpringEndpointSampleApplicationTests {

	public static final String[][] dataArray = {{"{1,15,5,7,3}", "{3,4,7,12,21,14,9,5}", "{1,2,0,9,5,4,3,2}", "{7,8,2,9,2,1,4,3}", "{4,5,0,1,2,4,7}"},
			{"{15, 7, 3, 5, 1}", "{7, 14, 21, 3, 5, 9, 12, 4}", "{3, 5, 9, 1, 2, 2, 4, 0}", "{7, 3, 9, 1, 2, 2, 4, 8}", "{7, 5, 1, 2, 4, 4, 0}"}}; // 1d -> data, 2d -> data Sorted

	private static final Logger log = LoggerFactory.getLogger(SpringEndpointSampleApplicationTests.class);

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void contextGetRequest() throws Exception {
		makeRequest(RequestMethod.GET);
	}

	@Test
	public void contextPOSTRequest() throws Exception {
		makeRequest(RequestMethod.POST);
	}

	/**
	 * Common method for make request
	 * @param type - POST or GET.
	 * @throws Exception
	 */
	private void makeRequest(RequestMethod type) throws Exception {
		for (int i = 0; i < dataArray[1].length; i++) {
			String data = dataArray[0][i];

			MockHttpServletRequestBuilder request;
			if (type.equals(RequestMethod.GET))
				request = get(EndPoints.WELCOME);
			else if (type.equals(RequestMethod.POST))
				request = post(EndPoints.WELCOME);
			else
				throw new IllegalArgumentException("RequestMethod not contemplated.");

			String response = mockMvc.perform(request.content(data).contentType(MediaType.TEXT_PLAIN_VALUE)).
					andExpect(status().is(HttpStatus.OK.value())).
					andReturn().getResponse().getContentAsString();

			String expectedResult = dataArray[1][i];

			String logMsg = longMsgPrint(data, expectedResult,response);
			log.info(logMsg);
		}
	}

	/**
	 * Returns formatted Msg for print in logger.
	 */
	private String longMsgPrint(String data, String expectedResult, String response){
		String isOK = response.equals(expectedResult) ? "isOK" : "isInvalid";
		return new StringBuilder("Input data: ").append(data).append("\n").
				append("Expected output: ").append(expectedResult).append("\n").
				append("Output Value:").append(response).
				append(" - ").append(isOK).append("\n").toString();
	}

}
