package org.sample.SpringEndpointSample.rest;

import org.sample.SpringEndpointSample.algorithms.SortTypes;
import org.sample.SpringEndpointSample.constants.Message;
import org.sample.SpringEndpointSample.utils.NumUtils;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class NumsService {
    private static String OPEN  = "{";
    private static String CLOSE = "}";

    public String sortNums(int nums[]) {
        int   len   = nums.length;
        int[] bnums = new int[len];

        for (int i = 0; i < len; i++) {
            int number = nums[i];
            bnums[i] = NumUtils.countBits(number);
        }

        SortTypes.insertionBinary(nums, bnums); // Sort both arrays.

        return intArraytoSting(nums);
    }

    /**
     * Format <code>int[]</code> to <code>String</code>
     * @param nums Array of ints.
     * @return String representation of array follow this format: ej {1,2,4}.
     */
    private String intArraytoSting(int[] nums){
        StringBuilder numBuilder = new StringBuilder("{");
        Arrays.toString(nums);
        int iMax = nums.length - 1;
        for (int i = 0; ; i++) {
            numBuilder.append(nums[i]);
            if (i == iMax)
                return numBuilder.append('}').toString();
            numBuilder.append(", ");
        }
    }

    /**
     * Valida los datos recibidos en la request y les da un formato valido si todo es correcto.
     *
     * @param data -> Datos enviados en la request.
     * @return Valid -> una <code>String</code> con una lista de numeros separados por comas. ej: 1,3,5,9.
     * <br> Invalid -> throws <code>IllegalArgumentException</code>.
     */
    public String validateData(String data) {
        String  dataValid = data.trim(); //comprobar los espacios en blanco
        boolean isValid   = dataValid.startsWith(OPEN) && dataValid.endsWith(CLOSE);

        if (isValid) { // remove open and close brackets.
            int idxClose = dataValid.indexOf(CLOSE);
            dataValid = new StringBuilder(dataValid).deleteCharAt(idxClose).deleteCharAt(0).toString();
        } else
            throw new IllegalArgumentException(Message.ERROR_OPEN_CLOSE);

        dataValid = dataValid.trim();
        isValid = !dataValid.isEmpty();
        if (dataValid.isEmpty()) {
            throw new IllegalArgumentException(Message.ERROR_EMPTY_DATA);
        }

        isValid = dataValid.matches("^(\\d+,?)*\\d+$");
        if (!isValid) {
            throw new IllegalArgumentException(Message.ERROR_INVALID_FORMAT);
        }

        return dataValid; // ALL OK.
    }
}
