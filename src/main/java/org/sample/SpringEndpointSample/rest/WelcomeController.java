package org.sample.SpringEndpointSample.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

@RestController
public class WelcomeController {
    private NumsService numService;

    @Autowired
    public WelcomeController(NumsService numService) {
        this.numService = numService;
    }

//    private int[] nums;

    // Sample: curl http://localhost:8080/welcome -X POST --data "{1,15,5,7,3}" -H "Content-Type: text/plain"
    @RequestMapping(value = "/welcome", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE)
    public String welcomePOST(@RequestBody String data) {
        return welcome(data);
    }

    // Sample: curl http://localhost:8080/welcome -X GET --data "{1,15,5,7,3}" -H "Content-Type: text/plain"
    @RequestMapping(value = "/welcome", method = RequestMethod.GET, consumes = MediaType.TEXT_PLAIN_VALUE)
    public String welcomeGET(@RequestBody String data) {
        return welcome(data);
    }

    public String welcome(String data) {
        String dataValid = numService.validateData(data);
        int[]  nums      = Arrays.stream(dataValid.split(",")).mapToInt(Integer::parseInt).toArray();
        int    len       = nums.length;


        return numService.sortNums(nums);
    }
}

