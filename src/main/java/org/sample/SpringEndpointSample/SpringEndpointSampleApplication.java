package org.sample.SpringEndpointSample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringEndpointSampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringEndpointSampleApplication.class, args);
	}

}
