package org.sample.SpringEndpointSample.algorithms;

public class SortTypes {
    /**
     * <p><strong>Time Complexity:</strong> O(n*2)</p>
     * <p><strong>Auxiliary Space:</strong> O(1)</p>
     * <p><strong>Stable:</strong> Yes</p>
     * Insertion sort used when number of elements is small.
     * @param num Array of nums.
     * @param bnum Binary Array representation of <strong>num</strong>.
     * @return sorted Array of nums.
     */
    public static void insertionBinary(int[] num, int[] bnum) {
        int   nlen       = num.length;

        for (int i = 1; i < nlen; i++) {
            int k1 = bnum[i];
            int k2 = num[i];

            int j = i - 1;
            while (j >= 0 && ((bnum[j] == k1 && num[j] > k2) || bnum[j] < k1)) {
                num[j + 1] = num[j];
                bnum[j + 1] = bnum[j];
                j = j - 1;
            }

            num[j + 1] = k2;
            bnum[j + 1] = k1;
        }
    }
}
