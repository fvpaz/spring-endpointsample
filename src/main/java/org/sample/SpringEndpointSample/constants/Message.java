package org.sample.SpringEndpointSample.constants;

public class Message {
    public final static String ERROR_EMPTY_DATA     = "No se han enviado datos.";
    public final static String ERROR_OPEN_CLOSE     = "Los datos no estan abiertos o cerrados correctamente.";
    public final static String ERROR_INVALID_FORMAT = "Los datos tienen un formato invalido.";
}
