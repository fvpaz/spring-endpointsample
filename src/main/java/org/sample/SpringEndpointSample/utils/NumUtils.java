package org.sample.SpringEndpointSample.utils;

public class NumUtils {
    /**
     * Count number bits in <code>int</code> number.
     * @param n <code>int</code>
     * @return number bits.
     */
    public static int countBits(int n) {
        int count = 0;

        while (n > 0) {
            if ((n & 1) >0)
                count += 1;
            n >>= 1;
        }
        return count;
    }
}
