### Spring-Boot Sample: Endpoints

Spring boot demo Project with endpoint sample.

**Description:**

Send Array of numbers and sorted by binaries values.

#### Build the Project
```
./gradlew build
```

#### Run the Project
```
./gradlew bootRun
```

#### Use the REST Service:

* **POST :**
```
curl localhost:8080/welcome -X POST --data "{1,15,5,7,3}" -H "Content-Type: text/plain"
```
* **GET :**
```
curl localhost:8080/welcome -X GET --data "{1,15,5,7,3}" -H "Content-Type: text/plain"
```

**Format Data:**  `{^(\d+,?)\*\d+$}`

* > {1,15,5,7,3}    // VALID
* > {1, 15,5,7, 3} // INVALID
* > {12,}         //  INVALID